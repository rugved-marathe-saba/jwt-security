package app.rugved.jwtdemo.jwtdemo;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;

@SpringBootApplication
public class JwtDemoApplication {

    public static void main(String[] args) throws Exception {
    	SpringApplication.run(JwtDemoApplication.class, args);
    }
}
