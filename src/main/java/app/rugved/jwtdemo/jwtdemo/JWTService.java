package app.rugved.jwtdemo.jwtdemo;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.util.Calendar;
import java.util.Date;

@Service
public class JWTService {

    private static final String KEY_HEADER_V1 = "-----BEGIN RSA PRIVATE KEY-----";
    private static final String KEY_HEADER_V2 = "-----BEGIN PRIVATE KEY-----";
    private static final String KEY_FOOTER_V1 = "-----END RSA PRIVATE KEY-----";
    private static final String KEY_FOOTER_V2 = "-----END PRIVATE KEY-----";

    public String removeExtraInformationFromKey(String privateKey, String version) {
        File privateKeyFile = new File(privateKey);
        String privateKeyContent;
        try {
            privateKeyContent = new String(Files.readAllBytes(privateKeyFile.toPath()));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        if (version.equalsIgnoreCase("v1")) {
            privateKeyContent = privateKeyContent.replaceAll("\\n", "")
                    .replaceAll("\\r", "")
                    .replace(KEY_HEADER_V1, "")
                    .replace(KEY_FOOTER_V1, "");
        } else {
            privateKeyContent = privateKeyContent.replaceAll("\\n", "")
                    .replaceAll("\\r", "")
                    .replace(KEY_HEADER_V2, "")
                    .replace(KEY_FOOTER_V2, "");
        }
        return privateKeyContent;
    }

    public Date getIssuedAtDate() {
        return new Date();
    }

    public Date getExpirationDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, 6);
        return cal.getTime();
    }

    public Key loadPrivateKeyV2(String privateKeyPath) {
        try {
            String privateKeyContent = removeExtraInformationFromKey(privateKeyPath, "V2");
            byte[] privateKeyBase64 = Base64.decodeBase64(privateKeyContent);
            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(privateKeyBase64);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePrivate(spec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public Key loadPrivateKeyForV1(String privateKeyPath) {
        try {
            String privateKeyContent = removeExtraInformationFromKey(privateKeyPath, "V1");
            byte[] privateKeyBase64 = Base64.decodeBase64(privateKeyContent);
            RSAPrivateCrtKeySpec spec = getRSAKeySpec(privateKeyBase64);
            KeyFactory factory = KeyFactory.getInstance("RSA");
            return factory.generatePrivate(spec);
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) { //$NON-NLS-1$
            throw new IllegalArgumentException(e);
        }
    }

    private RSAPrivateCrtKeySpec getRSAKeySpec(byte[] privateKeyBase64) {
        try {
            DerParser parser = new DerParser(privateKeyBase64);

            Asn1Object sequence = parser.read();
            if (sequence.getType() != DerParser.SEQUENCE)
                throw new IOException("Invalid DER: not a sequence"); //$NON-NLS-1$

            // Parse inside the sequence
            parser = sequence.getParser();
            parser.read(); // Skip version
            BigInteger modulus = parser.read().getInteger();
            BigInteger publicExp = parser.read().getInteger();
            BigInteger privateExp = parser.read().getInteger();
            BigInteger prime1 = parser.read().getInteger();
            BigInteger prime2 = parser.read().getInteger();
            BigInteger exp1 = parser.read().getInteger();
            BigInteger exp2 = parser.read().getInteger();
            BigInteger crtCoef = parser.read().getInteger();

            RSAPrivateCrtKeySpec keySpec = new RSAPrivateCrtKeySpec(
                    modulus, publicExp, privateExp, prime1, prime2,
                    exp1, exp2, crtCoef);
            return keySpec;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
