package app.rugved.jwtdemo.jwtdemo;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/jwt")
public class JWTController {

    @Autowired
    private JWTService jwtService;

    @GetMapping("/v1")
    public ResponseEntity<?> generateJwtV2(@RequestParam("account_name") String accountName,
                                           @RequestParam("private_key_path") String privateKeyPath,
                                           @RequestParam("public_key_url") String publicKeyUrl,
                                           @RequestParam(name = "department_code", required = false) String departmentCode) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("ACCOUNT_NAME", accountName);
        String jwtToken = Jwts.builder().setClaims(claims)
                .setHeaderParam("alg", "RS256")
                .setHeaderParam("typ", "JWT")
                .setHeaderParam("kid", "lumesse_internal_jwt")
                .setHeaderParam("x5u", publicKeyUrl)
                .setExpiration(jwtService.getExpirationDate())
                .setIssuedAt(jwtService.getIssuedAtDate())
                .signWith(SignatureAlgorithm.RS256, jwtService.loadPrivateKeyForV1(privateKeyPath))
                .compact();
        return new ResponseEntity<>(jwtToken, HttpStatus.OK);
    }

    @GetMapping("/v2")
    public ResponseEntity<?> generateJwtV1(@RequestParam("account_name") String accountName,
                                           @RequestParam("private_key_path") String privateKeyPath,
                                           @RequestParam("public_key_url") String publicKeyUrl,
                                           @RequestParam(name = "department_code", required = false) String departmentCode) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("ACCOUNT_NAME", accountName);
        String jwtToken = Jwts.builder().setClaims(claims)
                .setHeaderParam("alg", "RS256")
                .setHeaderParam("typ", "JWT")
                .setHeaderParam("kid", "lumesse_internal_jwt")
                .setHeaderParam("x5u", publicKeyUrl)
                .setExpiration(jwtService.getExpirationDate())
                .setIssuedAt(jwtService.getIssuedAtDate())
                .signWith(SignatureAlgorithm.RS256, jwtService.loadPrivateKeyV2(privateKeyPath))
                .compact();
        return new ResponseEntity<>(jwtToken, HttpStatus.OK);
    }
}
